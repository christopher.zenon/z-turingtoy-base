from typing import Dict, List, Optional, Tuple, Any
import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

def run_turing_machine(machine: Dict[str, Any], input_str: str, steps: Optional[int] = None) -> Tuple[str, List[Dict[str, Any]], bool]:
    blank = machine['blank']
    start_state = machine['start state']
    final_st = machine['final states']
    table = machine['table']

    band_input = list(input_str)
    cur_st = start_state
    pos_cur = 0                 # Position de la tête de lecture 
    history = []                # Historique
    exec_history = {}           # Historique execution
    leaving_state = 0           # Type d état en fin d algo


    def mov_cur(direction):
        nonlocal pos_cur
        if direction == 'L':
            pos_cur -= 1
            if pos_cur < 0:
                pos_cur = 0
                band_input.insert(0, blank)     # Créer un espace exploitable
        elif direction == 'R':
            pos_cur += 1
            if pos_cur == len(band_input):
                band_input.append(blank)
    
    def transition(state, symbol):
        return table[state][symbol]

    def read_symbol():
        if pos_cur < 0 or pos_cur >= len(band_input):
            return blank
        return band_input[pos_cur]

    def wrt_symbol(symbol):
        band_input[pos_cur] = symbol
        print("writing :", symbol)

    def execute_step(cur_st):
        symbol = read_symbol()

        exec_history = {
            "state": cur_st,
            "reading": symbol,
            "pos_cur": pos_cur,
            "memory": ''.join(band_input),
        }

        data_sw = transition(cur_st, symbol)
        exec_history["transition"] = data_sw

        if data_sw == "L":
            mov_cur('L')
        elif data_sw == "R":
            mov_cur('R')
        else:
            if "write" in data_sw:
                wrt_symbol(data_sw.get('write', blank))
                if leaving_state == 2:
                    return 2, cur_st, exec_history

            direction = 'L' if data_sw.get('L', '') else 'R'
            mov_cur(direction)
            next_st = data_sw.get(direction)

            if data_sw.get(direction, blank) in final_st:
                return 1, cur_st, exec_history
            if next_st:
                cur_st = next_st

        return 0, cur_st, exec_history

    for step in range(steps) if steps is not None else range(1000000):
        if cur_st in final_st:
            break

        leaving_state, cur_st, exec_output = execute_step(cur_st)
        history.append(exec_output.copy())

        if leaving_state != 0:
            leaving_state = True if leaving_state == 1 else False
            break

    output = ''.join(band_input)
    output = output.strip(blank)

    return output, history, leaving_state

def to_dict(keys: List[str], value: Any) -> Dict[str, Any]:
    return {key: value for key in keys}

machine = {
    "blank": " ",
    "start state": "right",
    "final states": ["done"],
    "table": {
        "right": {
            **to_dict(["0", "1", "+"], "R"),
            " ": {"L": "read"},
        },
        "read": {
            "0": {"write": "c", "L": "read_0"},
            "1": {"write": "c", "L": "read_1"},
            "+": {"write": " ", "L": "rewrite"},
        },
        "read_0": {**to_dict(["0", "1"], "L"), "+": {"L": "add0"}},
        "read_1": {**to_dict(["0", "1"], "L"), "+": {"L": "add1"}},
        "left_wr0": {
            **to_dict(["0", "1", "O", "I", "+"], "R"),
            "c": {"write": "0", "L": "read"},
        },
        "left_wr1": {
            **to_dict(["0", "1", "O", "I", "+"], "R"),
            "c": {"write": "1", "L": "read"},
        },
        "add0": {
            **to_dict(["0", " "], {"write": "O", "R": "left_wr0"}),
            "1": {"write": "I", "R": "left_wr0"},
            **to_dict(["O", "I"], "L"),
        },
        "add1": {
            **to_dict(["0", " "], {"write": "I", "R": "left_wr1"}),
            "1": {"write": "O", "L": "carry"},
            **to_dict(["O", "I"], "L"),
        },
        "carry": {
            **to_dict(["0", " "], {"write": "1", "R": "left_wr1"}),
            "1": {"write": "0", "L": "carry"},
        },
        "rewrite": {
            "O": {"write": "0", "L": "rewrite"},
            "I": {"write": "1", "L": "rewrite"},
            **to_dict(["0", "1"], "L"),
            " ": {"R": "done"},
        },
        "done": {},
    },
}

input_str = "11+1"
print(run_turing_machine(machine, input_str))
